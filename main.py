import sys,tty,termios

print("---------------------------------------")
print("             TREASURE HUNT ")
print("---------------------------------------")
print(" ")
print( "# # # # # # # #" )
print( "# . . . . . $ #" )
print( "# . # # # . . #" )
print( "# . . . # . # #" )
print( "# x # . . . . #" )
print( "# # # # # # # #" )
print(" ")
print("X Is start point ")
print("# Is obstacle ")
print(". Is a clear path ")
print("$ Posible treasure ")
print("---------------------------------------")


# Commands and escape codes
END_OF_TEXT = chr(3)  # CTRL+C (prints nothing)
END_OF_FILE = chr(4)  # CTRL+D (prints nothing)
CANCEL      = chr(24) # CTRL+X
ESCAPE      = chr(27) # Escape
CONTROL     = ESCAPE +'['

# Escape sequences for terminal keyboard navigation
ARROW_UP    = CONTROL+'A'
ARROW_DOWN  = CONTROL+'B'
ARROW_RIGHT = CONTROL+'C'
ARROW_LEFT  = CONTROL+'D'

# Escape sequences to match
commands = {
    ARROW_UP   :'up arrow',
    ARROW_DOWN :'down arrow',
    ARROW_RIGHT:'right arrow',
    ARROW_LEFT :'left arrow',
}

# Blocking read of one input character, detecting appropriate interrupts
def getchar():
    k = sys.stdin.read(1)[0]
    if k in {END_OF_TEXT, END_OF_FILE, CANCEL}: raise KeyboardInterrupt
    return k

# Preserve current terminal settings (we will restore these before exiting)
fd = sys.stdin.fileno()
old_settings = termios.tcgetattr(fd)

x = 1
y = 1

try:
    # Enter raw mode (key events sent directly as characters)
    tty.setraw(sys.stdin.fileno())

    # Loop, waiting for keyboard input
    while 1:
        read = getchar()
        while any(k.startswith(read) for k in commands.keys()):
            if read in commands:
                if x == 1 and y == 1 :
                    if commands[read] == 'up arrow':
                        y+=1
                    if commands[read] == 'down arrow':
                        print('obstacle')
                    if commands[read] == 'left arrow':
                        print('obstacle')
                    if commands[read] == 'right arrow':
                        print('obstacle')
                elif x == 1 and y == 2 :
                    if commands[read] == 'up arrow':
                        y+=1
                    if commands[read] == 'down arrow':
                        y-=1
                    if commands[read] == 'left arrow':
                        print('obstacle')
                    if commands[read] == 'right arrow':
                        x+=1
                elif x == 1 and y == 3 :
                    if commands[read] == 'up arrow':
                        y+=1
                    if commands[read] == 'down arrow':
                        y-=1
                    if commands[read] == 'left arrow':
                        print('obstacle')
                    if commands[read] == 'right arrow':
                        print('obstacle')
                elif x == 1 and y == 4 :
                    if commands[read] == 'up arrow':
                        print('obstacle')
                    if commands[read] == 'down arrow':
                        y-=1
                    if commands[read] == 'left arrow':
                        print('obstacle')
                    if commands[read] == 'right arrow':
                        x+=1
                elif x == 2 and y == 2 :
                    if commands[read] == 'up arrow':
                        print('obstacle')
                    if commands[read] == 'down arrow':
                        print('obstacle')
                    if commands[read] == 'left arrow':
                        x-=1
                    if commands[read] == 'right arrow':
                        x+=1
                elif x == 2 and y == 4 :
                    if commands[read] == 'up arrow':
                        print('obstacle')
                    if commands[read] == 'down arrow':
                        print('obstacle')
                    if commands[read] == 'left arrow':
                        x-=1
                    if commands[read] == 'right arrow':
                        x+=1
                elif x == 3 and y == 1 :
                    if commands[read] == 'up arrow':
                        y+=1
                    if commands[read] == 'down arrow':
                        print('obstacle')
                    if commands[read] == 'left arrow':
                        print('obstacle')
                    if commands[read] == 'right arrow':
                        x+=1
                elif x == 3 and y == 2 :
                    if commands[read] == 'up arrow':
                        print('obstacle')
                    if commands[read] == 'down arrow':
                        y-=1
                    if commands[read] == 'left arrow':
                        x-=1
                    if commands[read] == 'right arrow':
                        print('obstacle')
                elif x == 3 and y == 4 :
                    if commands[read] == 'up arrow':
                        print('obstacle')
                    if commands[read] == 'down arrow':
                        print('obstacle')
                    if commands[read] == 'left arrow':
                        x-=1
                    if commands[read] == 'right arrow':
                        x+=1
                elif x == 4 and y == 1 :
                    if commands[read] == 'up arrow':
                        print('obstacle')
                    if commands[read] == 'down arrow':
                        print('obstacle')
                    if commands[read] == 'left arrow':
                        x-=1
                    if commands[read] == 'right arrow':
                        x+=1
                elif x == 4 and y == 4 :
                    if commands[read] == 'up arrow':
                        print('obstacle')
                    if commands[read] == 'down arrow':
                        print('obstacle')
                    if commands[read] == 'left arrow':
                        x-=1
                    if commands[read] == 'right arrow':
                        x+=1
                elif x == 5 and y == 1 :
                    if commands[read] == 'up arrow':
                        y+=1
                    if commands[read] == 'down arrow':
                        print('obstacle')
                    if commands[read] == 'left arrow':
                        x-=1
                    if commands[read] == 'right arrow':
                        x+=1
                elif x == 5 and y == 2 :
                    if commands[read] == 'up arrow':
                        y+=1
                    if commands[read] == 'down arrow':
                        y-=1
                    if commands[read] == 'left arrow':
                        print('obstacle')
                    if commands[read] == 'right arrow':
                        print('obstacle')
                elif x == 5 and y == 3 :
                    if commands[read] == 'up arrow':
                        y+=1
                    if commands[read] == 'down arrow':
                        y-=1
                    if commands[read] == 'left arrow':
                        print('obstacle')
                    if commands[read] == 'right arrow':
                        x+=1
                elif x == 5 and y == 4 :
                    if commands[read] == 'up arrow':
                        print('obstacle')
                    if commands[read] == 'down arrow':
                        y-=1
                    if commands[read] == 'left arrow':
                        x-=1
                    if commands[read] == 'right arrow':
                        x+=1
                        print('Yeeaaaa !!! you get treasure on position (6, 2)')
                elif x == 6 and y == 1 :
                    if commands[read] == 'up arrow':
                        print('obstacle')
                    if commands[read] == 'down arrow':
                        print('obstacle')
                    if commands[read] == 'left arrow':
                        x-=1
                    if commands[read] == 'right arrow':
                        print('obstacle')
                elif x == 6 and y == 3 :
                    if commands[read] == 'up arrow':
                        y+=1
                        print('Yeeaaaa !!! you get treasure on position (6, 2)')
                    if commands[read] == 'down arrow':
                        print('obstacle')
                    if commands[read] == 'left arrow':
                        x-=1
                    if commands[read] == 'right arrow':
                        print('obstacle')
                elif x == 6 and y == 4 :
                    if commands[read] == 'up arrow':
                        print('obstacle')
                    if commands[read] == 'down arrow':
                        y-=1
                    if commands[read] == 'left arrow':
                        x-=1
                    if commands[read] == 'right arrow':
                        print('obstacle')
                read = ''
                break
            read += getchar()

finally:
    termios.tcsetattr(fd, termios.TCSADRAIN, old_settings) 
    sys.exit(0)